package controller;

import java.io.IOException;
import java.sql.Connection;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.JDBCNoteDAOImpl;
import dao.JDBCUsersNotesDAOImpl;
import dao.NoteDAO;
import dao.UsersNotesDAO;
import model.Note;

/**
 * Servlet implementation class EditNoteServlet
 */
@WebServlet("/Notes/EditNoteServlet")
public class EditNoteServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public EditNoteServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub

		
		HttpSession session = request.getSession();
	
		Connection conn = (Connection) getServletContext().getAttribute("dbConn");
		
		NoteDAO noteDao = new JDBCNoteDAOImpl();
		noteDao.setConnection(conn);
		
		UsersNotesDAO usernote=new JDBCUsersNotesDAOImpl();
		usernote.setConnection(conn);
		
		
		
//		Integer s=Integer.parseInt(request.getParameter("idn"));
		
		String c=request.getParameter("idn");//pillo idn de nota a editar
		
//		int x=((User) session.getAttribute("user")).getIdu();
		
//		System.out.println(c);
		
		Integer s=Integer.parseInt(c);
		
		session.setAttribute("idn", s.intValue());//lo coloco en la sesion
		
		Note note=noteDao.get(s.longValue());//y cojo la nota
		
		
//		request.setAttribute("note", note);
		
//		System.out.println(note.getContent());
		
		request.setAttribute("Title", note.getTitle());
		request.setAttribute("Content", note.getContent());
		
		
		RequestDispatcher view = request.getRequestDispatcher("/WEB-INF/EditNote.jsp");
		view.forward(request,response);
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		Connection conn = (Connection) getServletContext().getAttribute("dbConn");

		
		
		NoteDAO noteDao = new JDBCNoteDAOImpl();
		noteDao.setConnection(conn);
		
//		UsersNotesDAO usernote=new JDBCUsersNotesDAOImpl();
//		usernote.setConnection(conn);
		
		HttpSession session = request.getSession();
		
		
		
		int idn=(Integer)session.getAttribute("idn");
		
		Note note=noteDao.get(idn);
		
//		UsersNotes usrnte= (UsersNotes) session.getAttribute("usrNote");
		
		note.setContent(request.getParameter("Content"));
		note.setTitle(request.getParameter("Title"));
		
		
		noteDao.save(note);
		
		response.sendRedirect("ListUsersNotesServlet");
		
		
		
		
		
		
	}

}