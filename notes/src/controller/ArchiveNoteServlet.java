package controller;

import java.io.IOException;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.JDBCNoteDAOImpl;
import dao.JDBCUserDAOImpl;
import dao.JDBCUsersNotesDAOImpl;
import dao.NoteDAO;
import dao.UserDAO;
import dao.UsersNotesDAO;
import model.Note;
import model.User;
import model.UsersNotes;
import util.Pair;

/**
 * Servlet implementation class ArchiveNoteServlet
 */
@WebServlet("/Notes/ArchiveNoteServlet")
public class ArchiveNoteServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ArchiveNoteServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		Connection conn = (Connection) getServletContext().getAttribute("dbConn");
		UserDAO userDAO = new JDBCUserDAOImpl();
		userDAO.setConnection(conn);

		UsersNotesDAO usersNotesDAO = new JDBCUsersNotesDAOImpl();
		usersNotesDAO.setConnection(conn);
		
		NoteDAO noteDAO = new JDBCNoteDAOImpl();
		noteDAO.setConnection(conn);
		
		HttpSession session = request.getSession();
		
		User usr=(User) session.getAttribute("user");
		
		
		String c=request.getParameter("idn");//pillo idn de nota a archivar

		
		Integer s=Integer.parseInt(c);
		
		
		UsersNotes usrnote=usersNotesDAO.get(usr.getIdu(), s);//pillo la tupla user-note de la nota usuario
		
		
		if(usrnote.getArchived()==0){//si llego aqui con 0 quiero archivar 
			
			usrnote.setArchived(1);//la arch
			
			usersNotesDAO.save(usrnote);//y guardo
			
		}
		
		else{
			
			usrnote.setArchived(0);//la quito
			
			usersNotesDAO.save(usrnote);//y guardo
			
			
		}
		
		
		
		
		response.sendRedirect("ListUsersNotesServlet");// 

		
	
	}
		
		
		
		
	

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		
		Connection conn = (Connection) getServletContext().getAttribute("dbConn");
		UserDAO userDAO = new JDBCUserDAOImpl();
		userDAO.setConnection(conn);

		UsersNotesDAO usersNotesDAO = new JDBCUsersNotesDAOImpl();
		usersNotesDAO.setConnection(conn);
		
		NoteDAO noteDAO = new JDBCNoteDAOImpl();
		noteDAO.setConnection(conn);
		
		HttpSession session = request.getSession();
		
		User usr=(User) session.getAttribute("user");
		
		

		List <UsersNotes> usersNotesList = usersNotesDAO.getAllByUser(usr.getIdu());//lista de usernotes de toda las notas accesibles por el idU
//		
		Iterator<UsersNotes> itUsersNotesList = usersNotesList.iterator();
		
		List<Pair<UsersNotes, Note>> notasArch = new ArrayList<Pair<UsersNotes, Note>>();//creo lista de usernote/note

		
		while(itUsersNotesList.hasNext()) {
			
			UsersNotes usersNotes = (UsersNotes) itUsersNotesList.next();//creo una nueva id idn owner...
			
			Note note = noteDAO.get(usersNotes.getIdn());// pillo nota suya
			
			
			
			if(usersNotes.getArchived()==1)			
			notasArch.add(new Pair<UsersNotes, Note>(usersNotes, note));//añado un nuevo par de elmtos a la lista de pines

			
//			if(usersNotes.getPinned()==1){
//				
//				notaspin.add(new Pair<UsersNotes, Note>(usersNotes, note));//añado un nuevo par de elmtos a la lista de pines
//				
//				
//				
//			}
////			
//			else{
////			
//			notaspropias.add(new Pair<UsersNotes, Note>(usersNotes, note));//añado un nuevo par de elmtos
////			
//			}

			
					
		}
		
		
		request.setAttribute("notasArch",notasArch);
		
		RequestDispatcher view = request.getRequestDispatcher("/WEB-INF/ArchiveNote.jsp");
		view.forward(request,response);

		
	}
}
