package controller;

import java.io.IOException;
import java.sql.Connection;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.JDBCNoteDAOImpl;
import dao.JDBCUserDAOImpl;
import dao.JDBCUsersNotesDAOImpl;
import dao.NoteDAO;
import dao.UserDAO;
import dao.UsersNotesDAO;
import model.User;
import model.UsersNotes;

/**
 * Servlet implementation class PinNoteServlet
 */
@WebServlet("/Notes/PinNoteServlet")
public class PinNoteServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public PinNoteServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		doPost(request,response);
		
		
//		Note note=noteDao.get(s.longValue());//y cojo la nota
		
		
		
		
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		
		Connection conn = (Connection) getServletContext().getAttribute("dbConn");
		UserDAO userDAO = new JDBCUserDAOImpl();
		userDAO.setConnection(conn);

		UsersNotesDAO usersNotesDAO = new JDBCUsersNotesDAOImpl();
		usersNotesDAO.setConnection(conn);
		
		NoteDAO noteDao = new JDBCNoteDAOImpl();
		noteDao.setConnection(conn);
		
		HttpSession session = request.getSession();
		
		User usr=(User) session.getAttribute("user");
		
		
		
		String c=request.getParameter("idn");//pillo idn de nota a pinnear

		
		Integer s=Integer.parseInt(c);
		
//		session.setAttribute("idn", s.intValue());//lo coloco en la sesion
		
		UsersNotes usrnote=usersNotesDAO.get(usr.getIdu(), s);//pillo la tupla user-note de la nota usuario
		
		
		if(usrnote.getPinned()==0){//si llego aqui con 0 quiero pinearla 
			
			usrnote.setPinned(1);//la fijo
			
			usersNotesDAO.save(usrnote);//y guardo
			
		}
		
		else{
			
			usrnote.setPinned(0);//la quito
			
			usersNotesDAO.save(usrnote);//y guardo
			
			
		}
		
		
//		RequestDispatcher view = request.getRequestDispatcher("WEB-INF/ListUsersNotes.jsp");
//		view.forward(request,response);
		
		response.sendRedirect("ListUsersNotesServlet");// 

		
		
		
		
		
	}

}

//Simulación de Implementación del Caso de Uso 10, Tercer Commit