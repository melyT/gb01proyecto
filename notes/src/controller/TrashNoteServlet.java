package controller;

import java.io.IOException;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.JDBCNoteDAOImpl;
import dao.JDBCUserDAOImpl;
import dao.JDBCUsersNotesDAOImpl;
import dao.NoteDAO;
import dao.UserDAO;
import dao.UsersNotesDAO;
import model.Note;
import model.User;
import model.UsersNotes;
import util.Pair;

/**
 * Servlet implementation class TrashNoteServlet
 */
@WebServlet("/Notes/TrashNoteServlet")
public class TrashNoteServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public TrashNoteServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub

		Connection conn = (Connection) getServletContext().getAttribute("dbConn");
		UserDAO userDAO = new JDBCUserDAOImpl();
		userDAO.setConnection(conn);

		UsersNotesDAO usersNotesDAO = new JDBCUsersNotesDAOImpl();
		usersNotesDAO.setConnection(conn);
		
		NoteDAO noteDAO = new JDBCNoteDAOImpl();
		noteDAO.setConnection(conn);
		
		HttpSession session = request.getSession();
		
		User usr=(User) session.getAttribute("user");
		
		

		List <UsersNotes> usersNotesList = usersNotesDAO.getAllByUser(usr.getIdu());//lista de usernotes de toda las notas accesibles por el idU
//		
		Iterator<UsersNotes> itUsersNotesList = usersNotesList.iterator();
		
		List<Pair<UsersNotes, Note>> notetrash = new ArrayList<Pair<UsersNotes, Note>>();//creo lista de usernote/note

		
		while(itUsersNotesList.hasNext()) {
			
			UsersNotes usersNotes = (UsersNotes) itUsersNotesList.next();//creo una nueva id idn owner...
			
			Note note = noteDAO.get(usersNotes.getIdn());// pillo nota suya
			
			
			
			if(usersNotes.getTrashed()==1) {			
				notetrash.add(new Pair<UsersNotes, Note>(usersNotes, note));
				System.out.println("he a�adido "+note.getIdn()+" a la basura");//a�ado un nuevo par de elmtos a la lista de borradas

			}


			
					
		}
		
		
		System.out.println("Tama�o de la basura "+notetrash.size());
		
		
		request.setAttribute("notetrash",notetrash);
		
		RequestDispatcher view = request.getRequestDispatcher("/WEB-INF/TrashNotes.jsp");
		view.forward(request,response);

	
	
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		Connection conn = (Connection) getServletContext().getAttribute("dbConn");
		UserDAO userDAO = new JDBCUserDAOImpl();
		userDAO.setConnection(conn);

		UsersNotesDAO usersNotesDAO = new JDBCUsersNotesDAOImpl();
		usersNotesDAO.setConnection(conn);
		
		NoteDAO noteDao = new JDBCNoteDAOImpl();
		noteDao.setConnection(conn);
		
		HttpSession session = request.getSession();
		
		User usr=(User) session.getAttribute("user");
		
		
		
		String c=request.getParameter("idn");//pillo idn de nota a borrar

		
		Integer s=Integer.parseInt(c);
		
//		session.setAttribute("idn", s.intValue());//lo coloco en la sesion
		
		UsersNotes usrnote=usersNotesDAO.get(usr.getIdu(), s);//pillo la tupla user-note de la nota usuario
		
		
		if(usrnote.getTrashed()==0){//si llego aqui con 0 quiero borrarla
			
			System.out.println("Borrando ? la  nota "+usrnote.getIdn());
			
			usrnote.setPinned(0);//la desfijo si estuviera
			
			usrnote.setArchived(0);//la desarchivo si estuviera
			
			usrnote.setColour("N/D");//le quito el color

			
			usrnote.setTrashed(1);//la borro finalmente

			
			usersNotesDAO.save(usrnote);//y guardo
			
		}
		
		else{
			
			usrnote.setTrashed(0);//la recupero
			
			usrnote.setPinned(0);//la desfijo si estuviera
			
			usrnote.setArchived(0);//la desarchivo si estuviera
			
			usrnote.setColour("N/D");//le quito el color

			
			usersNotesDAO.save(usrnote);//y guardo
			
			
		}
		
		
		response.sendRedirect("ListUsersNotesServlet");		
		
		
		
		
		
	}

}
