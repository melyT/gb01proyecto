package controller;

import java.io.IOException;
import java.sql.Connection;
import org.apache.commons.codec.digest.DigestUtils;

import java.util.logging.Logger;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.JDBCUserDAOImpl;
import dao.UserDAO;
import model.User;

/**
 * Servlet implementation class LoginServlet
 */
@WebServlet(
		urlPatterns = { "/LoginServlet" }
		)
public class LoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static final Logger logger = Logger.getLogger(HttpServlet.class.getName());
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public LoginServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		HttpSession session = request.getSession();
		
		if (session.getAttribute("user")!= null) //si hay algun user redirect
				response.sendRedirect("ListUsersNotesServlet");// /orders filter
		else { 
				RequestDispatcher view = request.getRequestDispatcher("/WEB-INF/login.jsp");//si no login
				view.forward(request,response);
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		Connection conn = (Connection) getServletContext().getAttribute("dbConn");
		UserDAO userDao = new JDBCUserDAOImpl();
		userDao.setConnection(conn);
		
		String username = request.getParameter("username");
		String password = request.getParameter("password");		
		
		logger.info("credentials: "+username+" - "+password);
		
		User user = userDao.get(username);
		
		
		String crypt =DigestUtils.md5Hex(password);
		
		
		if ((user != null)&& (user.getPassword().equals(crypt))){
			
			HttpSession session = request.getSession();
			session.setAttribute("user",user);
			logger.info("En login, pass correct y redirect a listnotes");

			response.sendRedirect("Notes/ListUsersNotesServlet");// /orders
			
		} 
		else {
			
			request.setAttribute("messages","Wrong username or password!!");
			RequestDispatcher view = request.getRequestDispatcher("/WEB-INF/login.jsp");
			view.forward(request,response);
		}	
	}

}

