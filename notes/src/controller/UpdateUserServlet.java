package controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.util.logging.Logger;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.codec.digest.DigestUtils;

import dao.JDBCUserDAOImpl;
import dao.UserDAO;
import model.User;

/**
 * Servlet implementation class UpdateUserServlet
 */
@WebServlet(urlPatterns = { "/User/UpdateUserServlet"})

public class UpdateUserServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	private static final Logger logger = Logger.getLogger(HttpServlet.class.getName());

       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UpdateUserServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
	
		HttpSession session = request.getSession();
		
		User usr=(User) session.getAttribute("user");

	
		if (usr!= null) {//si hay algun user modificar
			
			
			
			request.setAttribute("messages","Edit user");
			
			
			
			request.setAttribute("username", usr.getUsername());
			request.setAttribute("password", usr.getPassword());
			request.setAttribute("email", usr.getEmail());
			
			

			RequestDispatcher view = request.getRequestDispatcher("/WEB-INF/EditUser.jsp");//si no login
			view.forward(request,response);
		
		}
		
	else { 
			RequestDispatcher view = request.getRequestDispatcher("/WEB-INF/login.jsp");//si no login
			view.forward(request,response);
	}
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		
		Connection conn = (Connection) getServletContext().getAttribute("dbConn");
		UserDAO userDao = new JDBCUserDAOImpl();
		userDao.setConnection(conn);
		
		HttpSession session = request.getSession();
		
		User usr=new User(request.getParameter("password"),request.getParameter("email"),request.getParameter("username"));
		
		System.out.println("valor de pass nueva-> "+ usr.getPassword());
		
		
		System.out.println("valor de pass antig-> "+((User) session.getAttribute("user")).getPassword());


		
		usr.setIdu(((User) session.getAttribute("user")).getIdu());
		
//		String pwd_old=
		
		logger.info("Updating user");
		PrintWriter out=response.getWriter();

	 if(!(usr.getPassword().trim().equals(((User) session.getAttribute("user")).getPassword().trim())) ){//si ha cambiado la encripto de nuevo
		 
		 
		 usr.setPassword(DigestUtils.md5Hex(request.getParameter("password")));
		 
	 }
		
			
			if(userDao.save(usr)){
				
				logger.info(usr.getUsername());
				
				
			

			out.print("<!DOCTYPE html>");
			
			out.print("<body>");
			
			out.print("<p>The User "+usr.getUsername()+" has been updated.");
			
			out.print("<p>You will redirect to login page in 5 seconds.");

			out.print("</body>");
			
			out.print("</html>");
			
			
			
			
			
			response.sendRedirect(request.getContextPath() + "/Notes/ListUsersNotesServlet");
			
			}
			
			else{
				
				
				
				
				request.setAttribute("messages","Error has ocurred in update of the user "+usr.getUsername());
				RequestDispatcher view = request.getRequestDispatcher("/WEB-INF/EditUser.jsp");
				view.forward(request,response);
				
				
				
			}

		
			
	}

}