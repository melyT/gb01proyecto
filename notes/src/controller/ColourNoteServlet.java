package controller;

import java.io.IOException;
import java.sql.Connection;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.JDBCNoteDAOImpl;
import dao.JDBCUserDAOImpl;
import dao.JDBCUsersNotesDAOImpl;
import dao.NoteDAO;
import dao.UserDAO;
import dao.UsersNotesDAO;
import model.Note;
import model.User;
import model.UsersNotes;

/**
 * Servlet implementation class ColourNoteServlet
 */
@WebServlet("/Notes/ColourNoteServlet")
public class ColourNoteServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ColourNoteServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		Connection conn = (Connection) getServletContext().getAttribute("dbConn");
		UserDAO userDAO = new JDBCUserDAOImpl();
		userDAO.setConnection(conn);
		
		UsersNotesDAO usersNotesDAO = new JDBCUsersNotesDAOImpl();
		usersNotesDAO.setConnection(conn);

		
		
		NoteDAO noteDao = new JDBCNoteDAOImpl();
		noteDao.setConnection(conn);
		
		HttpSession session = request.getSession();
		
		User usr=(User) session.getAttribute("user");
		
		
		
		String c=request.getParameter("idn");//pillo idn de nota a colorear
		
		Integer s=Integer.parseInt(c);

		
		String col=request.getParameter("color");//colorrrr
		
		UsersNotes usrnote=usersNotesDAO.get(usr.getIdu(), s);//pillo la tupla user-note de la nota usuario
		
		usrnote.setColour(col);
		
		usersNotesDAO.save(usrnote);
		
		response.sendRedirect("ListUsersNotesServlet");// 

		
		
		
		
		

		
		
		
		
		
	}

}

//Simulación de Implementación del Caso de Uso 11, Tercer Commit