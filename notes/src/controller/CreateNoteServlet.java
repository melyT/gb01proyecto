package controller;

import java.io.IOException;
import java.sql.Connection;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.JDBCNoteDAOImpl;
import dao.JDBCUsersNotesDAOImpl;
import dao.NoteDAO;
import dao.UsersNotesDAO;
import model.Note;
import model.User;
import model.UsersNotes;

/**
 * Servlet implementation class CreateNoteServlet
 */
@WebServlet("/Notes/CreateNoteServlet")
public class CreateNoteServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public CreateNoteServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		
		request.setAttribute("messages","Create a new note");

		RequestDispatcher view = request.getRequestDispatcher("/WEB-INF/CreateNote.jsp");
		view.forward(request,response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		Connection conn = (Connection) getServletContext().getAttribute("dbConn");
		
		NoteDAO noteDao = new JDBCNoteDAOImpl();
		noteDao.setConnection(conn);
		
		UsersNotesDAO usernote=new JDBCUsersNotesDAOImpl();
		usernote.setConnection(conn);
		
		HttpSession session = request.getSession();
		User s=(User) session.getAttribute("user");
		
		
		
		
		
		Note note=new Note(request.getParameter("Title"),request.getParameter("content"));
	
		note.setIdn((int) noteDao.add(note));
		
		System.out.println("Hasta aqui pepino ermano");

		
		UsersNotes NoteUser=new UsersNotes(s.getIdu() , note.getIdn(), 1, 0, 0,0,"none");
		
		System.out.println("yuuuuu Usernote "+NoteUser.getIdu()+NoteUser.getIdn());
		
		
		
		System.out.println(usernote.add(NoteUser));
		
		

		response.sendRedirect("ListUsersNotesServlet");
		
		
	}

}