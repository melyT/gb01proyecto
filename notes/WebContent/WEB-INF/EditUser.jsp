<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>

  <head>

    <meta charset="utf-8">

  <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/style.css">

    <title> edit </title>

  </head>

  <body>
  
  <p>${messages}</p>
  
  <div class="home">
		
		<a class="Home" href="${pageContext.request.contextPath}/Notes/ListUsersNotesServlet"><img src="${pageContext.request.contextPath}/img/casa.png" alt="Home"></a> 
		
		
		</div>


    <fieldset>
    
    <form action="UpdateUserServlet" method="post" > 

      <h1>  <legend> Personal details </legend> </h1>

        <label> <h3>Username</h3></label><br>
      
        <input type="text" name="username" value="${username}"> <br>

		
        <h3>Email</h3>
         <input type="email" name="email" value="${email}"><br>
         
         <div id="password">
         <h3>Password</h3>
			<input type="password" name="password" value="${password}">
		</div>

	
      </fieldset>
      
     

     

      <fieldset>
       	 <input type="submit" name="submit" value="Confirm"><br>
       
      </fieldset>
      
       </form>
       
       
       		  <li><a class="delete" href="${pageContext.request.contextPath}/User/DeleteUserServlet"><img src="${pageContext.request.contextPath}/img/deleteUser.png" alt="delete"></a></li>
       
      
  </body>


</html>