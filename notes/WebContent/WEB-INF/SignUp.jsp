<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>

  <head>

    <meta charset="utf-8">

  <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/reg.css">

    <title> Sign Up Notes </title>

  </head>

  <body>
  
  
    <div class="Encabezado">

      <div class="Interior">

        <ul>
          
          <li><a class="edit" href="${pageContext.request.contextPath}/LoginServlet">Inicio</a> </li>
          <li><a class="edit" href="${pageContext.request.contextPath}/SignUpServlet">Registro</a></li>


        </ul>
      </div>
    </div>



      <div class="principal">
      
      <p>${messages}</p>

	<div class="signup">
	<form id="signup" action="SignUpServlet" method="post">
    <h1>Register here!</h1>

    <input type="username" name="username" placeholder="your username" required="">
    <input type="email"  name="email" placeholder="john.doe@email.com" required="">
    <input type="password"  name="password" placeholder="Choose your password" required="">
    <input type="submit" name="submit" value="Confirm">   
 
</form>

</div>
</div>


  </body>
</html>