package model;

import static org.junit.Assert.*;

import org.junit.BeforeClass;
import org.junit.Test;

import model.User;
import model.UsersNotes;

public class TestUsersNotes {
	
	private static UsersNotes un;

	@BeforeClass
	public static void testUsersNotes() {
		
		 un = new UsersNotes();
		 un.setIdu(1);
		 un.setIdn(1);
		 un.setOwner(1); //0=false,1=true
		 un.setArchived(0); //0=false,1=true
		 un.setPinned(1); //0=false,1=true
		 un.setTrashed(0); //0=false,1=true
		 un.setColour("red");
		 
	}

	@Test
	public void testUsersNotesIntIntIntIntIntIntString() {
		
		assertNotNull(un.getIdu());
		assertNotNull(un.getIdn());
	}

	@Test
	public void testValidate() {
		assertEquals(un.getOwner(),1);
		assertNotEquals(un.getArchived(), 1);
		assertEquals(un.getPinned(), 1);
		assertNotEquals(un.getTrashed(), 2);
	}

	@Test
	public void testGetIdu() {
		
		assertNotNull(un.getIdu());
	}

	@Test
	public void testSetIdu() {
		
		assertNotEquals(un.getIdu(), 2);
	}

	@Test
	public void testGetIdn() {
		assertNotNull(un.getIdn());
	}

	@Test
	public void testSetIdn() {
		
		assertEquals(un.getIdn(), 1);
	}

	@Test
	public void testGetOwner() {
		assertNotNull(un.getOwner());
	}

	@Test
	public void testSetOwner() {
	
		assertEquals(un.getOwner(), 1);
	}

	@Test
	public void testGetArchived() {
		assertNotNull(un.getArchived());
	}

	@Test
	public void testSetArchived() {
		
		assertEquals(un.getArchived(), 0);
	}

	@Test
	public void testGetPinned() {
		assertNotNull(un.getPinned());
	}

	@Test
	public void testSetPinned() {
		
		assertEquals(un.getArchived(),0);
	}

	@Test
	public void testGetTrashed() {
		assertNotNull(un.getTrashed());
	}

	@Test
	public void testSetTrashed() {
	
		assertEquals(un.getTrashed(), 0);
	}

	@Test
	public void testGetColour() {
		assertNotNull(un.getColour());
	}

	@Test
	public void testSetColour() {
		
		assertNotEquals(un.getColour(), "blue");
	}

}
