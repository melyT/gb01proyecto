package model;

import static org.junit.Assert.*;

import org.junit.Test;

import model.User;

public class TestUser {

	
	
	
	
	@Test
	public void testUserStringStringString() {

		User user = new User("Admin", "123@123.com", "admin");
		assertNotNull(user);
		
	}

	@Test
	public void testUser() {
		User user1 = new User();
		int id =1;
		assertNotNull(user1);
	}

	@Test
	public void testValidate() {
		User user1 = new User();
		int id =1;
		
		user1.setUsername("a");
		assertFalse("Username length must be higher than 3 characters.", user1.getUsername().length() >3);
		user1.setUsername("admin");
		assertTrue(user1.getUsername().length() >3);
		
		user1.setPassword("123");
		assertFalse("Password length must be higher than 6 characters.", user1.getPassword().length() > 6);
		user1.setPassword("Admin2");
		assertFalse(user1.getPassword().length() > 6);
		
		
		user1.setEmail(" ");
		assertTrue("Email cannot have blank spaces.", user1.getEmail().contains(" "));
		user1.setEmail("123@123.com");
		assertFalse( user1.getEmail().contains(" "));
		
	}

	@Test
	public void testGetIdu() {
		User user1 = new User();
		int id =1;
		
		user1.setIdu(id);
		assertNotNull(user1.getIdu());
	}

	@Test
	public void testSetIdu() {
		User user1 = new User();
		int id =1;
		
		user1.setIdu(id);
		assertEquals(id,1);
	}

	@Test
	public void testGetUsername() {
		User user1 = new User();
		int id =1;
		
		user1.setUsername("admin");
		assertNotNull(user1.getUsername());
	}

	@Test
	public void testSetUsername() {
		User user1 = new User();
		int id =1;
		
		user1.setUsername("admin");
		assertEquals(user1.getUsername(),"admin");
	}

	@Test
	public void testGetPassword() {
		User user1 = new User();
		int id =1;
		
		user1.setPassword("Admin2");
		assertNotNull(user1.getPassword());
	}

	@Test
	public void testSetPassword() {
		User user1 = new User();
		int id =1;
		
		user1.setPassword("Admin2");
		assertEquals(user1.getPassword(),"Admin2");
	}

	@Test
	public void testGetEmail() {
		User user1 = new User();
		int id =1;
		
		user1.setEmail("123@123.com");
		assertNotNull(user1.getEmail());
	}

	@Test
	public void testSetEmail() {
		User user1 = new User();
		int id =1;
		
		user1.setEmail("123@123.com");
		assertEquals(user1.getEmail(),"123@123.com");
	}

}
