package Selenium;

import static org.junit.Assert.fail;

import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class UC13BuscarNota {
  private WebDriver driver;
  private String baseUrl;
  private boolean acceptNextAlert = true;
  private StringBuffer verificationErrors = new StringBuffer();

  @Before
  public void setUp() throws Exception {
	  System.setProperty("webdriver.gecko.driver", "test\\Selenium\\geckodriver.exe");

    driver = new FirefoxDriver();
    baseUrl = "https://www.katalon.com/";
    driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
  }

  @Test
  public void testUC13BuscarNota() throws Exception {
    driver.get("https://localhost:8443/notes-0.0.1-SNAPSHOT/LoginServlet");
    driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Inicio'])[1]/following::li[1]")).click();
    driver.findElement(By.linkText("Registro")).click();
    driver.findElement(By.name("username")).click();
    driver.findElement(By.name("username")).clear();
    driver.findElement(By.name("username")).sendKeys("Admin");
    driver.findElement(By.name("email")).clear();
    driver.findElement(By.name("email")).sendKeys("Admin@admin.com");
    driver.findElement(By.name("email")).sendKeys(Keys.DOWN);
    driver.findElement(By.name("email")).sendKeys(Keys.TAB);
    driver.findElement(By.name("password")).clear();
    driver.findElement(By.name("password")).sendKeys("Admin2");
    driver.findElement(By.name("submit")).click();
    driver.findElement(By.xpath("//div[@id='navbar14']/ul[2]/li[2]/a/i")).click();
    driver.findElement(By.name("Title")).click();
    driver.findElement(By.name("Title")).clear();
    driver.findElement(By.name("Title")).sendKeys("aaaaaaaaaaaaaaaa");
    driver.findElement(By.name("content")).click();
    driver.findElement(By.name("content")).clear();
    driver.findElement(By.name("content")).sendKeys("aaaaaaaaaaaaaaaa");
    driver.findElement(By.name("submit")).click();
    driver.findElement(By.xpath("//div[@id='navbar14']/ul[2]/li[2]/a/i")).click();
    driver.findElement(By.name("Title")).click();
    driver.findElement(By.name("Title")).clear();
    driver.findElement(By.name("Title")).sendKeys("bbbbbbbbbbbbbbb");
    driver.findElement(By.name("content")).click();
    driver.findElement(By.name("content")).clear();
    driver.findElement(By.name("content")).sendKeys("bbbbbbbbbbbbbbbbbbbbb");
    driver.findElement(By.name("submit")).click();
    driver.findElement(By.id("inlineFormInputGroup")).click();
    driver.findElement(By.id("inlineFormInputGroup")).clear();
    driver.findElement(By.id("inlineFormInputGroup")).sendKeys("aaa");
    driver.findElement(By.id("inlineFormInputGroup")).sendKeys(Keys.ENTER);
    driver.findElement(By.linkText("Home")).click();
    driver.findElement(By.id("inlineFormInputGroup")).click();
    driver.findElement(By.id("inlineFormInputGroup")).clear();
    driver.findElement(By.id("inlineFormInputGroup")).sendKeys("bbb");
    driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='NOTES'])[1]/following::div[14]")).click();
    driver.findElement(By.xpath("//input[@value='Search']")).click();
  }

  @After
  public void tearDown() throws Exception {
    driver.quit();
    String verificationErrorString = verificationErrors.toString();
    if (!"".equals(verificationErrorString)) {
      fail(verificationErrorString);
    }
  }

  private boolean isElementPresent(By by) {
    try {
      driver.findElement(by);
      return true;
    } catch (NoSuchElementException e) {
      return false;
    }
  }

  private boolean isAlertPresent() {
    try {
      driver.switchTo().alert();
      return true;
    } catch (NoAlertPresentException e) {
      return false;
    }
  }

  private String closeAlertAndGetItsText() {
    try {
      Alert alert = driver.switchTo().alert();
      String alertText = alert.getText();
      if (acceptNextAlert) {
        alert.accept();
      } else {
        alert.dismiss();
      }
      return alertText;
    } finally {
      acceptNextAlert = true;
    }
  }
}
