package Selenium;
import java.util.regex.Pattern;
import java.util.concurrent.TimeUnit;
import org.junit.*;
import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;
import org.openqa.selenium.*;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Select;

public class UC14BorrarColor {
  private WebDriver driver;
  private String baseUrl;
  private boolean acceptNextAlert = true;
  private StringBuffer verificationErrors = new StringBuffer();

  @Before
  public void setUp() throws Exception {
	  System.setProperty("webdriver.gecko.driver", "test\\Selenium\\geckodriver.exe");

    driver = new FirefoxDriver();
    baseUrl = "https://www.katalon.com/";
    driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
  }

  @Test
  public void testUC14BorrarColor() throws Exception {
    driver.get("https://localhost:8443/notes-0.0.1-SNAPSHOT/");
    driver.findElement(By.linkText("Registro")).click();
    driver.findElement(By.name("username")).click();
    driver.findElement(By.name("username")).clear();
    driver.findElement(By.name("username")).sendKeys("Ad");
    driver.findElement(By.name("username")).sendKeys(Keys.DOWN);
    driver.findElement(By.name("username")).clear();
    driver.findElement(By.name("username")).sendKeys("Admin");
    driver.findElement(By.name("email")).click();
    driver.findElement(By.name("email")).clear();
    driver.findElement(By.name("email")).sendKeys("AD");
    driver.findElement(By.name("email")).sendKeys(Keys.DOWN);
    driver.findElement(By.name("email")).clear();
    driver.findElement(By.name("email")).sendKeys("admin@admin.com");
    driver.findElement(By.name("password")).click();
    driver.findElement(By.name("password")).clear();
    driver.findElement(By.name("password")).sendKeys("Admin2");
    driver.findElement(By.name("submit")).click();
    driver.findElement(By.xpath("//div[@id='navbar14']/ul[2]/li[2]/a/i")).click();
    driver.findElement(By.name("Title")).click();
    driver.findElement(By.name("Title")).clear();
    driver.findElement(By.name("Title")).sendKeys("aaaaaaaaaaaaaaa");
    driver.findElement(By.name("content")).click();
    driver.findElement(By.name("content")).clear();
    driver.findElement(By.name("content")).sendKeys("bbbbbbbbbbbbbbb");
    driver.findElement(By.name("submit")).click();
    driver.findElement(By.name("color")).click();
    new Select(driver.findElement(By.name("color"))).selectByVisibleText("black");
    driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='aaaaaaaaaaaaaaa'])[1]/following::option[3]")).click();
    driver.findElement(By.xpath("(//input[@name='idn'])[4]")).click();
    driver.findElement(By.name("color")).click();
    new Select(driver.findElement(By.name("color"))).selectByVisibleText("none");
    driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='aaaaaaaaaaaaaaa'])[1]/following::option[4]")).click();
    driver.findElement(By.xpath("(//input[@name='idn'])[4]")).click();
  }

  @After
  public void tearDown() throws Exception {
    driver.quit();
    String verificationErrorString = verificationErrors.toString();
    if (!"".equals(verificationErrorString)) {
      fail(verificationErrorString);
    }
  }

  private boolean isElementPresent(By by) {
    try {
      driver.findElement(by);
      return true;
    } catch (NoSuchElementException e) {
      return false;
    }
  }

  private boolean isAlertPresent() {
    try {
      driver.switchTo().alert();
      return true;
    } catch (NoAlertPresentException e) {
      return false;
    }
  }

  private String closeAlertAndGetItsText() {
    try {
      Alert alert = driver.switchTo().alert();
      String alertText = alert.getText();
      if (acceptNextAlert) {
        alert.accept();
      } else {
        alert.dismiss();
      }
      return alertText;
    } finally {
      acceptNextAlert = true;
    }
  }
}
