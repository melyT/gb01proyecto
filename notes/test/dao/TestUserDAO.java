package dao;
import static org.junit.Assert.assertEquals;

import java.sql.Connection;

import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import model.User;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)

public class TestUserDAO {

	static DBConn dbConn;
	static UserDAO userDAO;
	static Connection conn;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		dbConn = new DBConn();
		conn = dbConn.create();
	    userDAO = new JDBCUserDAOImpl();
		userDAO.setConnection(conn);
	}
	
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		
		userDAO.deleteAll();
		
		userDAO.resetSeq();
		
		dbConn.destroy(conn);
	    
	}

	@Before
	public void setUpBeforeMethod() throws Exception {
		
	
		
		User user01 = new User( "vader", "darth.vader@darksideoftheforce.org", "darh");
		
		User user02 = new User( "solo", "han.solo@lightsideoftheforce.org", "han");
				
		User user03 = new User( "organa", "leia.organa@lightsideoftheforce.org", "leia");
			
		userDAO.add(user01);		
		userDAO.add(user02);
		userDAO.add(user03);
	}

	@Test
	public void test1BaseData() {
		User user1 = userDAO.get(1);
		assertEquals(user1.getIdu(),1);
		assertEquals(user1.getUsername(),"darh");

		User user2 = userDAO.get(2);
		assertEquals(user2.getIdu(),2);
		assertEquals(user2.getEmail(),"han.solo@lightsideoftheforce.org");
		
		User user3 = userDAO.get(3);
		assertEquals(user3.getIdu(),3);
		assertEquals(user3.getPassword(),"organa");
		
	}
	
	
	@Test
	public void test2Add(){
		User user04 = new User();
		user04.setUsername("newUser");
		user04.setEmail("newUser@unex.es");
		user04.setPassword("12345");
		userDAO.add(user04);
		
		User user05 = userDAO.get("newUser");
		assertEquals(user04.getEmail(),user05.getEmail());
		assertEquals(user04.getPassword(),user05.getPassword());
		

	}
	
	@Test
	public void test3Modify(){
		User user01 = userDAO.get("newUser");
		user01.setUsername("newUserUpdated");
		userDAO.save(user01);
		
		User user02 = userDAO.get("newUserUpdated");		
		assertEquals(user01.getUsername(),user02.getUsername());
		
	}
	
	@Test
	public void test4Delete(){
		 User user01 = userDAO.get("newUserUpdated");
		 userDAO.delete(user01.getIdu());
		 
		 User user02 = userDAO.get("newUserUpdated");
 		 assertEquals(null, user02);
 		 
 		userDAO.getAll();
	}

}
